# Getting started with the project
To clean and build the project run:
```json
$ mvn clean
```
```json
$ mvn compile
```
To run the feature tests you can either execute the TestRunner or run:
```json
$ mvn integration-test
```

To add new feature tests:
1. create a file with feature extension inside src/test/resources/features
2. implement the undefined step definitions inside a class from src/test/java/starter/stepdefinitions

# Refactoring scope
1. renamed files name for more alignment with the project scope
2. re-worded the step definitions to improve clarity
3. update params names to make code readable
4. added new scenarios, step definitions for more test coverage
5. organize java code in new classes according to the scope, group the logic and delineate responsibility of each unit of code