package starter.helpers;

import net.serenitybdd.rest.SerenityRest;

import java.util.List;
import java.util.Map;

public class JsonResponseHelper {
    public static List<Map<String, ?>> getProductsListFromJsonResponse(){
        return SerenityRest.lastResponse().jsonPath().getList("");
    }

    public static Map<String, ?> getJsonNode(String node) {
        return SerenityRest.lastResponse().jsonPath().get(node);
    }

}
