package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class EndpointRequestStepDefinitions {
    @When("i call endpoint {string}")
    public void iCallEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("request is processed with {int} status code")
    public void requestIsProcessedWithStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }
}
