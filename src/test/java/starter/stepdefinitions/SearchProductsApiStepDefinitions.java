package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import starter.helpers.JsonResponseHelper;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

public class SearchProductsApiStepDefinitions {

    @Then("i should receive a list of products")
    public void iShouldReceiveAListOfProducts() {
        List<Map<String, ?>> listOfProducts = JsonResponseHelper.getProductsListFromJsonResponse();

        assertThat("list is not empty", listOfProducts.size(), greaterThan(0));
    }

    @Then("the response body for an item has the following attributes")
    public void theResponseBodyForAnItemHasFollowingAttributes(DataTable attributes) {
        Map<String, ?> productItemMap = JsonResponseHelper.getProductsListFromJsonResponse().get(0);

        attributes.asList().forEach(
                attribute -> assertThat(attribute +" is not present", productItemMap.containsKey(attribute))
        );
    }

    @Then("the {string} node in response body has the following attributes")
    public void theDetailsNodeInResponseBodyHasFollowingAttributes(String node, DataTable attributes) {
        Map<String, ?> data = JsonResponseHelper.getJsonNode(node);

        attributes.asList().forEach(attribute ->
                assertThat(attribute +" is not present", data.containsKey(attribute)));
    }

    @Then("the response body contains only {string} attribute")
    public void theResponseBodyHasFollowingAttributes(String attribute) {
        Map<String, ?> data = JsonResponseHelper.getJsonNode("");

        assertThat(attribute + " not found", data.containsKey(attribute));
        assertThat("response contains more than 1 key", data.keySet().size(), equalTo(1));
    }
}
