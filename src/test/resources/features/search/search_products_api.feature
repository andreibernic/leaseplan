Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Retrieve an available product
    When i call endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<product>"
    Then request is processed with 200 status code
    And i should receive a list of products
    And the response body for an item has the following attributes
      | provider | title | url | brand | price | unit | isPromo | promoDetails | image |
    Examples:
      | product |
      | apple |
      | orange |
      | pasta |
      | cola |

  Scenario Outline: Attempt to retrieve an unavailable product
    When i call endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<product>>"
    Then request is processed with 404 status code
    And the "detail" node in response body has the following attributes
      | error | message | requested_item | served_by |
    Examples:
      | product |
      | car      |
      | animal   |


  Scenario: Attempt to retrieve an inexistent resource
    When i call endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/deom/car"
    Then request is processed with 404 status code
    And the response body contains only "detail" attribute

